<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8"/>
	<title>CinemaTicket</title>
	<link rel="stylesheet" type="text/css" href="../../css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Exo+2" rel="stylesheet"> 
	<meta name="decription" content="Wykorzystaj nasze narzędzie do rezerwacji biletów!" />
	<meta name="keywords" content="film, kino, bilety" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
</head>

<body background="css/foto.jpg">
	<div id="container">


		<div id="menu">
			<a href="/"><div class="option">HOME</div></a>
			<a href="/repertoire"><div class="option">REPERTOIRE</div></a>
			<a href="/priceList"><div class="option">PRICE LIST</div></a>
			<a href="/movieChoose"><div class="option">RESERVATION</div></a>
			<a href="/contact"><div class="option">CONTACT</div></a>
			<div class="clear"></div>
		</div>
		
		<div id="left">LEFT</div>
		
		<div id="center">

			<br>

			<h2>SUMMARY</h2>

			Check if your personal details have been written correct:

			<br><br>

			<table id="summary" border="1">
				<tr>
					<td>
						MOVIE:
					</td>
					<td>
						${reservation.movie}
					</td>
				</tr>

				<tr>
					<td>
						DATE:
					</td>
					<td>
						${reservation.date}
					</td>
				</tr>

				<tr>
					<td>
						TIME:
					</td>
					<td>
						${reservation.time}
					</td>
				</tr>

				<tr>
					<td>
						FIRST NAME:
					</td>
					<td>
						${reservation.firstName}
					</td>
				</tr>

				<tr>
					<td>
						LAST NAME:
					</td>
					<td>
						${reservation.lastName}
					</td>
				</tr>

				<tr>
					<td>
						E-MAIL:
					</td>
					<td>
						${reservation.mail}
					</td>
				</tr>

				<tr>
					<td>
						PHONE NUMBER:
					</td>
					<td>
						${reservation.phoneNumber}
					</td>
				</tr>
			</table>

			<br><br>

			<form:form action="confirm" modelAttribute="reservation">

				<form:hidden path="mail" />

				<a href="/personalData"><input type="button" value="BACK"></a>

				<input type="submit" value="CONFIRM">

			</form:form>

		</div>
		
		<div id="right">
			<div class="logButton">LOGIN</div>
			<div class="logButton">REGISTER</div>
		</div>
		
		<div id="footer">FOOTER</div>
		
	</div>
</body>
</html>