<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="DATE" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html lang="pl">
<head>
    <meta charset="utf-8"/>
    <title>CinemaTicket</title>
    <link rel="stylesheet" type="text/css" href="../../css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Exo+2" rel="stylesheet">
    <meta name="decription" content="Use our tool to book your cinema tickets!" />
    <meta name="keywords" content="movie, cinema, tickets" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
</head>

<body background="css/foto.jpg">

    <div id="container">


        <div id="menu">
            <a href="/"><div class="option">HOME</div></a>
            <a href="/repertoire"><div class="option">REPERTOIRE</div></a>
            <a href="/priceList"><div class="option">PRICE LIST</div></a>
            <a href="/movieChoose"><div class="option">RESERVATION</div></a>
            <a href="/contact"><div class="option">CONTACT</div></a>
            <div class="clear"></div>
        </div>

        <div id="left">LEFT</div>

        <div id="center">

        <br><br><br><br><br>



            <form:form action="personalData" modelAttribute="reservation">

                MOVIE:

                <br>

                <form:select path="movie">
                    <form:options items="${reservation.movieOptions}" />
                </form:select>

                <br><br>

                DATE:

                <br>

                <input name="date" type="date">

                <br><br>

                TIME:

                <br>
                <form:select path="time">
                    <form:options items="${reservation.timeOptions}" />
                </form:select>

                <br><br><br>

                <a href="/"><input type="button" value="BACK"></a>

                <input type="submit" value="SUBMIT">

            </form:form>

            <br><br><br>

        </div>

        <div id="right">
            <div class="logButton">LOGIN</div>
            <div class="logButton">REGISTER</div>
        </div>

        <div id="footer">FOOTER</div>

    </div>

</body>
</html>