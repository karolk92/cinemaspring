<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8"/>
	<title>CinemaTicket</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Exo+2" rel="stylesheet"> 
	<meta name="decription" content="Wykorzystaj nasze narzędzie do rezerwacji biletów!" />
	<meta name="keywords" content="film, kino, bilety" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
</head>

<body background="css/foto.jpg">

	<style>.error{color: darkblue}</style>

	<div id="container">


		<div id="menu">
			<a href="/"><div class="option">HOME</div></a>
			<a href="/repertoire"><div class="option">REPERTOIRE</div></a>
			<a href="/priceList"><div class="option">PRICE LIST</div></a>
			<a href="/movieChoose"><div class="option">RESERVATION</div></a>
			<a href="/contact"><div class="option">CONTACT</div></a>
			<div class="clear"></div>
		</div>
		
		<div id="left">LEFT</div>
		
		<div id="center"><br/>
			<form:form action="formResults" modelAttribute="reservation">
			</br>

				<form:hidden path="movie" />
				<form:hidden path="date" />
				<form:hidden path="time" />

				FIRST NAME</br>
				<form:input path="firstName" />
				<br>
				<form:errors path="firstName" cssClass="error" />
				<br/><br/>
				
				LAST NAME</br>
				<form:input path="lastName" />
				<br>
				<form:errors path="lastName" cssClass="error" />
				<br/><br/>
				
				E-MAIL</br>
				<form:input path="mail" />
				<br>
				<form:errors path="mail" cssClass="error" />
				<br/><br/>
				
				REPEAT E-MAIL</br>
				<form:input path="mail1" />
				<br>
				<form:errors path="mail1" cssClass="error" />
				<br/><br/>
				
				PHONE NUMBER</br>
				<form:input path="phoneNumber" />
				<br>
				<form:errors path="phoneNumber" cssClass="error" />
				<br/><br/><br/>

				<a href="/movieChoose"><input type="button" value="BACK"></a>

				<input type="submit" value="BOOKING">

			</form:form>
		</div>
		
		<div id="right">
			<div class="logButton">LOGIN</div>
			<div class="logButton">REGISTER</div>
		</div>
		
		<div id="footer">FOOTER</div>
		
	</div>
</body>
</html>