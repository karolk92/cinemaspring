<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8"/>
	<title>CinemaTicket</title>
	<link rel="stylesheet" type="text/css" href="css/style.css"/>
	<link href="https://fonts.googleapis.com/css?family=Exo+2" rel="stylesheet"> 
	<meta name="decription" content="Wykorzystaj nasze narzędzie do rezerwacji biletów!" />
	<meta name="keywords" content="film, kino, bilety" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
</head>

<body background="css/foto.jpg">
	<div id="container">


		<div id="menu">
			<a href="/"><div class="option">HOME</div></a>
			<a href="/repertoire"><div class="option">REPERTOIRE</div></a>
			<a href="/priceList"><div class="option">PRICE LIST</div></a>
			<a href="/movieChoose"><div class="option">RESERVATION</div></a>
			<a href="/contact"><div class="option">CONTACT</div></a>
			<div class="clear"></div>
		</div>
		
		<div id="left">LEFT</div>
		
		<div id="center">
			</br>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. In est lorem, suscipit a lacus aliquet, pharetra elementum elit. Curabitur luctus iaculis tortor, eu elementum erat commodo nec. Nullam sit amet vulputate urna. Integer sollicitudin sagittis nibh, eget iaculis ex ornare quis. Sed eu ultricies nisl. Vivamus fermentum arcu varius, consectetur diam in, fringilla libero. Etiam varius in erat eu imperdiet. Maecenas porta luctus semper. Suspendisse potenti. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec venenatis massa vel elit congue, ut efficitur elit consectetur. Nullam egestas velit tortor, auctor interdum neque interdum finibus.

			Donec porta porta tellus, id ultrices purus malesuada a. Pellentesque accumsan eros eu nulla convallis fermentum. Duis eu semper nisi. Cras eget nunc ante. Curabitur varius risus ut sollicitudin porttitor. Nullam metus augue, iaculis quis tellus sit amet, molestie facilisis nibh. Donec nec lacinia mi.

			Donec pretium laoreet blandit. Ut id diam nec sapien vestibulum lacinia et vitae risus. Praesent tempus velit augue, a imperdiet tortor tempus eu. Phasellus ac aliquam ante, at iaculis enim. Nam pellentesque urna velit, eu varius tellus elementum et. Suspendisse a mollis lacus. Donec dapibus ipsum vel mauris semper egestas. Nunc pellentesque diam nisi, vel imperdiet justo fermentum at. Suspendisse et nulla eget lacus varius imperdiet.

			Phasellus commodo eu erat sed rutrum. Curabitur nec felis magna. Donec pharetra tempor euismod. Sed ac molestie nisi. Duis eget augue posuere, vehicula leo quis, tristique eros. Praesent vitae faucibus tortor. Nullam vitae nibh in augue lacinia iaculis nec id nibh. Aenean facilisis dui in nibh ornare, id iaculis purus porta. Phasellus in hendrerit nibh. Nam eu lacinia orci. Aliquam erat volutpat. Pellentesque sollicitudin purus ac euismod dignissim.

			Morbi tristique sapien quis nisi iaculis, sed congue nunc tincidunt. Pellentesque eleifend finibus diam a efficitur. Phasellus suscipit sed nulla non ultrices. Nam tempus ante ut bibendum accumsan. Curabitur fringilla molestie auctor. Cras viverra risus sit amet magna ultrices consequat. Vivamus commodo molestie leo. Mauris varius nec libero vitae vehicula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nunc sit amet lacinia arcu. Sed venenatis porttitor nulla, non congue justo eleifend convallis. Nullam sed pharetra massa. Aenean condimentum risus vitae euismod blandit. Cras placerat vestibulum mauris nec convallis. Donec commodo tortor tincidunt nulla finibus auctor. 
		</div>
		
		<div id="right">
			<div class="logButton">LOGIN</div>
			<div class="logButton">REGISTER</div>
		</div>
		
		<div id="footer">FOOTER</div>
		
	</div>
</body>
</html>