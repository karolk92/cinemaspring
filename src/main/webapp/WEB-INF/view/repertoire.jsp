<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8"/>
	<title>CinemaTicket</title>
	<link rel="stylesheet" type="text/css" href="../../css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Exo+2" rel="stylesheet"> 
	<meta name="decription" content="Wykorzystaj nasze narzędzie do rezerwacji biletów!" />
	<meta name="keywords" content="film, kino, bilety" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
</head>

<body background="css/foto.jpg">
	<div id="container">


		<div id="menu">
			<a href="/"><div class="option">HOME</div></a>
			<a href="/repertoire"><div class="option">REPERTOIRE</div></a>
			<a href="/priceList"><div class="option">PRICE LIST</div></a>
			<a href="/movieChoose"><div class="option">RESERVATION</div></a>
			<a href="/contact"><div class="option">CONTACT</div></a>
			<div class="clear"></div>
		</div>
		
		<div id="left">LEFT</div>
		
		<div id="center">
			<div class="movieL"><img src="css/ass.jpg"/></div>
			<div class="movieC"><img src="css/baby.jpg"/></div>
			<div class="movieR"><img src="css/kingsman.jpg"/></div>
			<div class="movieL"><img src="css/spiderman.jpg"/></div>
			<div class="movieC"><img src="css/wizard.jpg"/></div>
			<div class="movieR"><img src="css/apes.jpg"/></div>
			<div class="clear"></div>
			
		</div>
		
		<div id="right">
			<div class="logButton">LOGIN</div>
			<div class="logButton">REGISTER</div>
		</div>
		
		<div id="footer">FOOTER</div>
		
	</div>
</body>
</html>