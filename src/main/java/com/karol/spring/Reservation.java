package com.karol.spring;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;


public class Reservation {

    LinkedHashMap<String, String> movieOptions;
    LinkedHashMap<String, String> timeOptions;


    public Reservation() {
        movieOptions = new LinkedHashMap<>();

        movieOptions.put("American Assassin", "American Assassin");
        movieOptions.put("Baby Driver", "Baby Driver");
        movieOptions.put("Kingsman", "Kingsman");
        movieOptions.put("Spiderman: Homecoming", "Spiderman: Homecoming");
        movieOptions.put("The wizard of lies", "The wizard of lies");
        movieOptions.put("War for the Planet of the Apes", "War for the Planet of the Apes");

        timeOptions = new LinkedHashMap<>();

        timeOptions.put("10:00", "10:00");
        timeOptions.put("12:00", "12:00");
        timeOptions.put("14:00", "14:00");
        timeOptions.put("16:00", "16:00");
        timeOptions.put("18:00", "18:00");
        timeOptions.put("20:00", "20:00");


    }

    private String movie;

    private LocalDateTime date;

    private String time;

    @Pattern(regexp = "^[a-zA-Z]{1,15}", message = "INVALID FIRST NAME")
    private String firstName;

    @Pattern(regexp = "^[a-zA-z]{1,15}", message = "INVALID LAST NAME")
    private String lastName;

    @Size(min=1, message="IS REQUIRED")
    @Email(message = "THAT'S NOT AN E-MAIL")
    private String mail;

    @Size(min=1, message="IS REQUIRED")
    @Email(message = "THAT'S NOT AN E-MAIL")
    private String mail1;

    @Pattern(regexp = "^[0-9-]{9,11}", message = "WRONG NUMBER LENGTH")
    private String phoneNumber;

    public LinkedHashMap<String, String> getMovieOptions() {
        return movieOptions;
    }

    public void setMovieOptions(LinkedHashMap<String, String> movieOptions) {
        this.movieOptions = movieOptions;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMovie() {
        return movie;
    }

    public void setMovie(String movie) {
        this.movie = movie;
    }

    public LinkedHashMap<String, String> getTimeOptions() {
        return timeOptions;
    }

    public void setTimeOptions(LinkedHashMap<String, String> timeOptions) {
        this.timeOptions = timeOptions;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMail1() {
        return mail1;
    }

    public void setMail1(String mail1) {
        this.mail1 = mail1;
    }
}
