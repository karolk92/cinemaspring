package com.karol.spring.com.karol.spring.controller;

import com.karol.spring.Reservation;
import com.karol.spring.com.karol.spring.util.MailService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.time.LocalDateTime;

@Controller
public class CinemaController {

    @RequestMapping("/")
    public String showIndex(){
        return "index";
    }

    @RequestMapping("/priceList")
    public String showPriceList(){
        return "priceList";
    }

    @RequestMapping("/repertoire")
    public String showRepertoire(){
        return "repertoire";
    }

    @RequestMapping("/contact")
    public String showContact(){
        return "contact";
    }

    @RequestMapping("/movieChoose")
    public String showMovieChoose(Model theModel){

        Reservation theReservation = new Reservation();

        theModel.addAttribute("reservation", theReservation);

        return "movieChoose";
    }

    @RequestMapping("/personalData")
    public String showPersonalForm(@ModelAttribute("reservation") Reservation theReservation,
                                   Model theModel,
                                   @RequestParam("movie") String movie,
                                   @RequestParam("date") LocalDateTime date,
                                   @RequestParam("time") String time) {

        return "personalData";
    }

    @RequestMapping("/formResults")
    public String showResults(@Valid @ModelAttribute("reservation") Reservation theReservation,
                              BindingResult theBindingResult,
                              @RequestParam("firstName") String firstName,
                              @RequestParam("lastName") String lastName,
                              @RequestParam("mail") String mail,
                              @RequestParam("mail1") String mail1,
                              @RequestParam("phoneNumber") String phoneNumber) {
        if(theBindingResult.hasErrors() || !mail.equals(mail1)) {
            return "personalData";
        } else {
            return "results";
        }
    }

    @RequestMapping("/confirm")
    public String showConfirmation(@ModelAttribute("reservation") Reservation theReservation) {

        MailService ms = new MailService();
        ms.sendMailTo(theReservation.getMail());

        return "emailSent";
    }
}
