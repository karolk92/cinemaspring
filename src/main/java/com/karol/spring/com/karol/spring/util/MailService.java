package com.karol.spring.com.karol.spring.util;

import com.karol.spring.Reservation;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class MailService {
    Reservation r = new Reservation();

    // DATA TO SEND AN E-MAIL
    String from = "cinnematic@gmail.com";
    String password = "cinema123";
    String subject = "Let's confirm your reservation!";
    String body = "Hello, you've ordered a tickets for a movie. Let's accept your order and enjoy nice movie! " +
            "\n" +
            "\n" +
            "Check your personal details:" +
            "\n" +
            "MOVIE: " + r.getMovie() +
            "\n" +
            "DATE: " + r.getDate() +
            "\n" +
            "TIME: " + r.getTime() +
            "\n" +
            "FIRST NAME: " + r.getFirstName() +
            "\n" +
            "LAST NAME: " + r.getLastName() +
            "\n" +
            "E-MAIL: " + r.getMail() +
            "\n" +
            "PHONE NUMBER: " + r.getPhoneNumber() +
            "\n" +
            "\n" +
            "Click the link below to confirm your order!";

    public void sendMailTo(String to) {
        Properties props = System.getProperties();
        String host = "smtp.gmail.com";

        props.put("mail.smtp.starttls.enable", "true");

        props.put("mail.smtp.ssl.trust", host);
        props.put("mail.smtp.user", from);
        props.put("mail.smtp.password", password);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");


        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {

            message.setFrom(new InternetAddress(from));

            InternetAddress toAddress = new InternetAddress(to);

            message.addRecipient(Message.RecipientType.TO, toAddress);

            message.setSubject(subject);
            message.setText(body);

            Transport transport = session.getTransport("smtp");


            transport.connect(host, from, password);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();

            System.out.println("E-mail sent correctly...");

        }
        catch (AddressException ae) {
            ae.printStackTrace();
        }
        catch (MessagingException me) {
            me.printStackTrace();
        }
    }
}
